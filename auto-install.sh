#!/bin/sh

mkdir /tmp/linux-install
cd /tmp/linux-install
curl -L https://gitlab.com/fridezlucas/linux-utilities/-/archive/master/linux-utilities-master.tar.gz | tar -xz
cd linux-utilities-master
sh install.sh
cd ~
rm -r /tmp/linux-install
