# Linux Utilities
This project contains all utilities for Linux.

## File System

### Architecture
Create base architecture in OS :
- `$HOME/DEV`
- `$HOME/HE-Arc`
- `$HOME/HE-Arc/Bachelor2`
- `$HOME/HE-Arc/Bachelor3`
- `$HOME/Documents/Utilities`
- `$HOME/Documents/Divers`
- `$HOME/Documents/Projects`
- `$HOME/Documents/FCBure`
- `$HOME/Documents/FCBure/MarcheGourmande`
- `$HOME/Documents/Holidays`
- `$HOME/Documents/Fonts`

### Nautilus preferences
Change nautilus Bookmarks :  
Add :
- `$HOME/DEV`
- `$HOME/HE-Arc/Bachelor2` as `He-Arc`

Remove :
- Music
- Vidéos
- Images
- Desktop


## Dependencies
Install some dependencies :
- `git`

## Programs
Install some programs :
- `Visual Studio Code`

## Themes
<table>
    <thead>
        <tr>
            <th>No°</th>
            <th>Gnome 3 Theme</th>
            <th>Shelle theme</th>
            <th>Icon Pack</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Qogir Theme</td>
            <td>Papirus</td>
            <td>Qogir Theme</td>
        </tr>
    </tbody>
</table>

## Author
<table>
   <tr>
      <td>
         <a href=https://gitlab.com/fridezlucas><img width=140px src=https://secure.gravatar.com/avatar/2e509c1fa696d7b33cf81f3addba7d78?s=180&d=identicon><br>
         Lucas Fridez</a><br>
         <i>Developer</i>
      </td>
   </tr>
</table>
&copy; 2019 Lucas Fridez
