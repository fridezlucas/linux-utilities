# GNOME utilities
dependencies=(
    "gnome-tweak-tool"
    "gnome-shell-extensions"
)

for dependency in ${dependencies[@]}
do
    sudo apt-get install "$dependency" -y
done

# Install theme
git clone https://github.com/vinceliuice/Orchis-theme.git
cd Orchis-theme
chmod +x ./install.sh
./install.sh
cd ../

# Dash to dock
git clone https://github.com/micheleg/dash-to-dock.git
cd dash-to-dock
make
make install
cd ..

# Icons
git clone https://github.com/vinceliuice/Tela-circle-icon-theme.git
cd Tela-circle-icon-theme
chmod +x ./install.sh
./install.sh
cd ..

# shell : update top bar (#panel) from Orchis

gnome-extensions enable user-theme@gnome-shell-extensions.gcampax.github.com


gsettings set org.gnome.desktop.interface gtk-theme "Orchis-compact"
gsettings set org.gnome.desktop.interface icon-theme "Tela-circle"
gsettings set org.gnome.desktop.wm.preferences theme "CoolestThemeOnEarth"

gsettings set org.gnome.shell.app-switcher current-workspace-only true
gsettings set org.gnome.shell.extensions.dash-to-dock isolate-workspaces true
