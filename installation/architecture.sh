# Create folders
folders=(
    "$HOME/DEV"
    "$HOME/DEV/tests"
    "$HOME/DEV/artionet"
    "$HOME/DEV/hes"
    "$HOME/HE-Arc"
    "$HOME/HE-Arc/Bachelor2"
    "$HOME/HE-Arc/Bachelor3"
    "$HOME/Documents/Utilities"
    "$HOME/Documents/Divers"
    "$HOME/Documents/Projects"
    "$HOME/Documents/FCBure"
    "$HOME/Documents/FCBure/MarcheGourmande"
    "$HOME/Documents/Holidays"
    "$HOME/Documents/Fonts"
)

for folder in ${folders[@]}
do
    mkdir -p "$folder"
done

# Remove unused folders from favorites
removed_favorites=(
    "Music"
    "Vidéos"
    "Images"
    "Desktop" #?
)

# Add new folders in favorites
add_folders=(
    "$HOME/DEV"
    "$HOME/HE-Arc/Bachelor3"
)