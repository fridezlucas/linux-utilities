# Add PPA
ppas=(
    "ppa:ubuntu-toolchain-r/ppa"
    "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
)

for ppa in ${ppas[@]}
do
    sudo add-apt-repository "$ppa"
done

curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -

# Update apt
sudo apt update -y

# Install dependencies & programs
dependencies=(
    "git"
    "software-properties-common"
    "python3.8"
    "apt-transport-https"
    "wget"
    "php"
    "php-pear" "php-fpm" "php-dev" "php-zip" "php-curl" "php-xmlrpc" "php-gd" "php-mysql" "php-mbstring" "php-xml" "libapache2-mod-php"
    "nodejs"
    "plantuml"
)

for dependency in ${dependencies[@]}
do
    sudo apt-get install "$dependency" -y
done

# Gulp and Typescript
npm install -g gulp typescript

