#!/bin/bash

# Update all packages
sudo apt update -y
sudo apt update -y

printf "*****************************\n"
printf "* Fridez Lucas\n"
printf "* Dell XPS 9570 installation\n"
printf "*****************************\n\n"

chmod +x "$(pwd)/installation" -R

printf "1. Architecture Creation..."
"$(pwd)/installation/architecture.sh"
printf " done !\n"

printf "2. Dependencies installation..."
"$(pwd)/installation/dependencies.sh"
printf " done !\n"

printf "3. Programs installation"
"$(pwd)/installation/programs.sh"
printf " done !\n"

printf "4. Themes installation ..."
"$(pwd)/installation/themes.sh"
printf " done !\n"